### Hi there 👋
My name is Jonathan Ling and I'm a software engineer at TVA. My responsibilities there include:
- Front end UI development with Angular 18
- Back end API development with C# and ASP.NET
- Scripting tasks I don't want to do more than once
- Posting links to cool new tech in Teams
- ADO pipeline creation and maintenance

In my spare time I like to:
* Work on open source software projects like:
  - [Node-Virtualization/node-virtualbox](https://github.com/Node-Virtualization/node-virtualbox)
  - [colonelpopcorn/Rippr](https://gitlab.com/colonelpopcorn/rippr)
  - [My personal home server scripts](https://github.com/colonelpopcorn/home-server)
  - [My sandbox/learning repository](https://github.com/colonelpopcorn/sandbox)

<!--
**colonelpopcorn/colonelpopcorn** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on ...
- 🌱 I’m currently learning ...
- 👯 I’m looking to collaborate on ...
- 🤔 I’m looking for help with ...
- 💬 Ask me about ...
- 📫 How to reach me: ...
- 😄 Pronouns: ...
- ⚡ Fun fact: ...
-->
